package com.jeecg.qywx.api.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 企业微信--menu
 * 
 * @author zhoujf
 * 
 */
public class JwUserAPI {
	private static final Logger logger = LoggerFactory.getLogger(JwUserAPI.class);

	//1 创建成员 
	private static String user_create_url = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=ACCESS_TOKEN";  
	//2 更新成员  
	private static String user_update_url = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN";  
	//3 删除成员
	private static String user_delete_url = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=ACCESS_TOKEN&userid=USERID";  
	//4 批量删除成员
	private static String user_delete_all_url = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=ACCESS_TOKEN";  
	//5 获取成员
	private static String user_get_url_byuserid = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID";  
	//6 获取部门下的成员
	private static String user_get_dep_all_url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD&status=STATUS";  
	//7 获取部门成员(详情)
	private static String user_get_url = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD&status=STATUS";  
	
	
	
	
	
	
}
